package fr.biblioc.mail.mail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MailSenderServiceImplTest {

    Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String EMAIL = "test@test.com";
    private static final String BODY = "Some contents.";
    private static final String SUBJECT = "Some subject";

    @Mock
    private JavaMailSender javaMailSender;

    @Mock
    MimeMessageHelper helper;

    @Autowired
    private MailSenderServiceImpl mailSender;

//    @Rule
//    public ErrorCollector collector = new ErrorCollector();

    @BeforeEach
    public void before() {
        MockitoAnnotations.initMocks(this);
        mailSender = new MailSenderServiceImpl(javaMailSender);
    }

    @Test
    public void send_should_return_exception() throws MessagingException {
        mailSender.setHtmlFormat(false);
        mailSender.send(EMAIL, SUBJECT, BODY);

        // Act
        //when(helper.setText(anyString(), anyBoolean())).thenReturn()

        // Assert
        assertThrows(Exception.class, () -> {mailSender.send(EMAIL, SUBJECT, BODY);});
    }

}

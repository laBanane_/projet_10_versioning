package fr.biblioc.bibliocreservation.web.controller.automation;

import fr.biblioc.bibliocreservation.dto.ExemplaireDto;
import fr.biblioc.bibliocreservation.dto.ListeAttenteDto;
import fr.biblioc.bibliocreservation.model.PreReservation;
import fr.biblioc.bibliocreservation.model.PrereservationDispoDataBean;
import fr.biblioc.bibliocreservation.web.controller.ExemplaireController;
import fr.biblioc.bibliocreservation.web.controller.ListeAttenteController;
import fr.biblioc.bibliocreservation.web.controller.PreReservationController;
import fr.biblioc.bibliocreservation.web.controller.automation.model.CompteBean;
import fr.biblioc.bibliocreservation.web.controller.automation.model.UtilisateurBean;
import fr.biblioc.bibliocreservation.web.controller.automation.proxies.BibliocAuthentificationProxy;
import fr.biblioc.bibliocreservation.web.controller.automation.proxies.BibliocBibliothequeProxy;
import fr.biblioc.bibliocreservation.web.controller.automation.proxies.BibliocMailProxy;
import fr.biblioc.bibliocreservation.web.controller.automation.proxies.BibliocUtilisateurProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller utilisant le proxy vers le microservice reservation
 */
@Controller
public class MailController {

    //------------------------- PARAMETRE -------------------------

    @Autowired
    private BibliocMailProxy mailProxy;

    @Autowired
    private BibliocAuthentificationProxy authentificationProxy;

    @Autowired
    private BibliocUtilisateurProxy utilisateurProxy;

    @Autowired
    private BibliocBibliothequeProxy bibliothequeProxy;

    @Autowired
    private PreReservationController preReservationController;

    @Autowired
    private ExemplaireController exemplaireController;

    @Autowired
    private ListeAttenteController listeAttenteController;

    Logger log = LoggerFactory.getLogger(this.getClass());

    //------------------------- METHODE -------------------------

    /**
     * Vérifie si il y a d'autre préreservation en attente par inspectionDelais et envoi la liste au microservice mail
     */
    public void inspectionDelaisAndSendMail() {
        //recuperer la liste de prereservation en attente de réponse expiré
        List<PreReservation> preReservationList = preReservationController.getExpiredMailSendPreReservation();

        if (preReservationList.size()!= 0) {
            sendToMail(inspectionListeAttente(preReservationList));
        }
    }

    /**
     * Vérifie après une préréservation expiré, si il y a une autre préréservation dans la liste d'attente si oui âsse au suivant si non rend l'exemplaire disponible
     * @return
     */
    public List<PrereservationDispoDataBean> inspectionListeAttente(List<PreReservation> preReservationList) {
            // RegleGestion 4
            //liste des données d'envoi de mail
            List<PrereservationDispoDataBean> prereservationDispoDataList = new ArrayList<>();

            for (PreReservation preReservation : preReservationList) {

                //expire la préréservation et l'update dans la bdd
                expirationAndUpdatePrereservation(preReservation);

                ListeAttenteDto listeAttenteDto = listeAttenteController.listAttenteById_liste_attente(preReservation.getId_liste_attente());

                List<PreReservation> preReservations = new ArrayList<>();
                for (PreReservation preReservation1 : listeAttenteDto.getPreReservationList()){
                    if (!preReservation1.isExpire()){
                        preReservations.add(preReservation1);
                    }
                }
                listeAttenteDto.setPreReservationList(preReservations);

                ExemplaireDto exemplaire = exemplaireController.recupererUnExemplaire(preReservation.getId_exemplaire());
                String titre = bibliothequeProxy.getTitreLivre(exemplaire.getId_livre());

                //Vérification de l'état de la liste d'attente
                if (!listeAttenteDto.getPreReservationList().isEmpty()) {

                    //suivant de la liste
                    PreReservation nextPreReservation = listeAttenteDto.getPreReservationList().get(0);
                    CompteBean compte = authentificationProxy.getCompte(preReservation.getId_compte());
                    UtilisateurBean utilisateur = utilisateurProxy.getUtilisateur(compte.getId_utilisateur());

                    //envoi de mail
                    PrereservationDispoDataBean prereservationDispoData = new PrereservationDispoDataBean(
                            preReservation.getId_exemplaire(),
                            compte.getEmail(),
                            utilisateur.getPrenom(),
                            exemplaire.getBibliotheque().getNom(),
                            titre);
                    prereservationDispoDataList.add(prereservationDispoData);

                    //mise a jour de la prereservation avec date envoi mail et l'id de l'exemplaire en plus
                    nextPreReservation.setId_exemplaire(preReservation.getId_exemplaire());
                    nextPreReservation.setDateMailSend(newDate());
                    preReservationController.updatePreReservation(nextPreReservation);

                } else {
                    exemplaire.setDisponible(true);
                    log.info("exemplaire du else de inspectionDelais : ", exemplaire);
                    exemplaireController.updateExemplaire(exemplaireController.getExemplaireFromDto(exemplaire));
                }
            }
            return prereservationDispoDataList;
    }


    public void expirationAndUpdatePrereservation(PreReservation preReservation){
        preReservation.setExpire(true);
        preReservationController.updatePreReservation(preReservation);
    }

    public void sendToMail (List<PrereservationDispoDataBean> prereservationDispoDataList){
        mailProxy.sendMailPrereservationList(prereservationDispoDataList);
    }

    //------------------------- METHODE INTERNE-------------------------

    /**
     * Renvoie la date du jour
     *
     * @return java.sql.Date
     */
    private Date newDate() {
        LocalDate localDate = LocalDate.now();

        return Date.valueOf(localDate);
    }
}

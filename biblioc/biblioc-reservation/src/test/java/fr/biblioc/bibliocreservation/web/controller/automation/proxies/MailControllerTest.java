package fr.biblioc.bibliocreservation.web.controller.automation.proxies;

import fr.biblioc.bibliocreservation.dao.PreReservationDao;
import fr.biblioc.bibliocreservation.dto.ExemplaireDto;
import fr.biblioc.bibliocreservation.dto.ListeAttenteDto;
import fr.biblioc.bibliocreservation.dto.PreReservationDto;
import fr.biblioc.bibliocreservation.model.*;
import fr.biblioc.bibliocreservation.web.controller.ExemplaireController;
import fr.biblioc.bibliocreservation.web.controller.ListeAttenteController;
import fr.biblioc.bibliocreservation.web.controller.PreReservationController;
import fr.biblioc.bibliocreservation.web.controller.automation.MailController;
import fr.biblioc.bibliocreservation.web.controller.automation.model.CompteBean;
import fr.biblioc.bibliocreservation.web.controller.automation.model.UtilisateurBean;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MailControllerTest {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Spy
    @InjectMocks
    private MailController mailController;

    @Mock
    private PreReservationDao preReservationDao;

    @Mock
    private PreReservationController preReservationController;

    @Mock
    private ExemplaireController exemplaireController;

    @Mock
    private ListeAttenteController listeAttenteController;

    @Mock
    private BibliocBibliothequeProxy bibliothequeProxy;

    @Mock
    private BibliocAuthentificationProxy authentificationProxy;

    @Mock
    private BibliocUtilisateurProxy utilisateurProxy;

    private List<PreReservation> preReservationsDummy;
    private List<PreReservationDto> preReservationDtosDummy;
    private List<PrereservationDispoDataBean> prereservationDispoDatasDummy;
    private ListeAttenteDto listeAttenteDtoDummy;

    @BeforeEach
    public void init(){
        preReservationsDummy = getPreReservationsDummy();
        preReservationDtosDummy = getPreReservationDtosDummy();
        prereservationDispoDatasDummy = getPreReservationsDispoDataDummy();
        listeAttenteDtoDummy = getListeAttenteDtoDummy();

    }

    @Test
    public void inspectionDelais_should_return_list_of_PrereservationDispoDataBean(){
        //GIVEN
        Bibliotheque bibliothequeDummy = new Bibliotheque("nom biblio", new Adresse());
        ExemplaireDto exemplaireDtoDummy = new ExemplaireDto(1, bibliothequeDummy, true);
        CompteBean compteDummy = new CompteBean("test@test.com", "1234", 3, 1);
        UtilisateurBean utilisateurDummy = new UtilisateurBean("nom de famille", "toto", 12345789, 1);

        //WHEN
        when(listeAttenteController.listAttenteById_liste_attente(anyInt())).thenReturn(listeAttenteDtoDummy);
        when(exemplaireController.recupererUnExemplaire(anyInt())).thenReturn(exemplaireDtoDummy);
        when(preReservationController.updatePreReservation(any())).thenReturn(null);
        when(bibliothequeProxy.getTitreLivre(anyInt())).thenReturn("un titre");
        when(authentificationProxy.getCompte(anyInt())).thenReturn(compteDummy);
        when(utilisateurProxy.getUtilisateur(anyInt())).thenReturn(utilisateurDummy);

        mailController.inspectionListeAttente(preReservationsDummy);

        List<PrereservationDispoDataBean> prereservationDispoDataBeans = mailController.inspectionListeAttente(preReservationsDummy);

        //THEN
        assertThat(prereservationDispoDataBeans).isNotEmpty();
    }

    @Test
    public void expirationPrereservation_should_modifiy_and_update(){
        //GIVEN
        PreReservation preReservationDummy = getPreReservationsDummy().get(0);
        //WHEN
        when( preReservationController.updatePreReservation(any())).thenReturn(null);
        mailController.expirationAndUpdatePrereservation(preReservationDummy);

        //THEN
        assertThat(preReservationDummy.isExpire()).isEqualTo(true);
    }

    private List<PrereservationDispoDataBean> getPreReservationsDispoDataDummy() {
        List<PrereservationDispoDataBean> prereservationDispoDataBeans = new ArrayList<>();
        PrereservationDispoDataBean prereservationDispoDataBean1 = new PrereservationDispoDataBean(1, "toto@toto.com", "toto", "bibliotest", "un bouquin");
        PrereservationDispoDataBean prereservationDispoDataBean2 = new PrereservationDispoDataBean(2, "tata@tata.com", "tata", "biblioexperience", "un autre bouquin");
        prereservationDispoDataBeans.add(prereservationDispoDataBean1);
        prereservationDispoDataBeans.add(prereservationDispoDataBean2);

        return prereservationDispoDataBeans;
    }

    @NotNull
    protected List<PreReservation> getPreReservationsDummy() {
        PreReservation preReservation1 = new PreReservation(1, Date.from(Instant.now()), 1, false, true);
        preReservation1.setId_prereservation(1);
        preReservation1.setId_exemplaire(1);
        PreReservation preReservation2 = new PreReservation(1, Date.from(Instant.now()), 2, false, true);
        preReservation1.setId_prereservation(1);
        preReservation2.setId_exemplaire(3);
        List<PreReservation> preReservationList = new ArrayList<PreReservation>();
        preReservationList.add(preReservation1);
        preReservationList.add(preReservation2);
        return preReservationList;
    }

    @NotNull
    protected List<PreReservationDto> getPreReservationDtosDummy() {
        PreReservationDto preReservationDto1 = new PreReservationDto(1, 1, Date.from(Instant.now()), 1, false, true);
        preReservationDto1.setId_exemplaire(1);
        PreReservationDto preReservationDto2 = new PreReservationDto(2,1, Date.from(Instant.now()), 2, false, true);
        preReservationDto2.setId_exemplaire(3);
        List<PreReservationDto> preReservationListDto = new ArrayList<PreReservationDto>();
        preReservationListDto.add(preReservationDto1);
        preReservationListDto.add(preReservationDto2);
        return preReservationListDto;
    }

    private ListeAttenteDto getListeAttenteDtoDummy() {
        ListeAttenteDto listeAttenteDto = new ListeAttenteDto();
        listeAttenteDto.setId_bibliotheque(1);
        listeAttenteDto.setId_liste_attente(1);
        listeAttenteDto.setId_livre(1);
        listeAttenteDto.setNbreExemplaire(4);
        PreReservation preReservation1 = new PreReservation(1, Date.from(Instant.now()), 1, false, false);
        preReservation1.setId_prereservation(1);
        preReservation1.setId_exemplaire(1);
        listeAttenteDto.addToList(preReservation1);

        return listeAttenteDto;
    }
}

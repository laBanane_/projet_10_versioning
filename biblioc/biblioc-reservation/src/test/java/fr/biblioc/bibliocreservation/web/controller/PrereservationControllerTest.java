package fr.biblioc.bibliocreservation.web.controller;

import fr.biblioc.bibliocreservation.dao.PreReservationDao;
import fr.biblioc.bibliocreservation.dto.PreReservationDto;
import fr.biblioc.bibliocreservation.mapper.PreReservationMapperImpl;
import fr.biblioc.bibliocreservation.model.PreReservation;
import fr.biblioc.bibliocreservation.web.exceptions.ErrorAddException;
import fr.biblioc.bibliocreservation.web.exceptions.FunctionalException;
import org.jetbrains.annotations.NotNull;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PrereservationControllerTest {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Spy
    @InjectMocks
    PreReservationController preReservationController;

    @Mock
    PreReservationDao preReservationDao;

    @Mock
    PreReservationMapperImpl mapper;

    @Test
    public void listeDesPreReservations_should_return_a_list() throws FunctionalException {
        //GIVEN
        List<PreReservation> preReservationList = getPreReservationsDummy();
        List<PreReservationDto> preReservationListDto = getPreReservationDtosDummy();

        when(preReservationController.getPrereservationDao()).thenReturn(preReservationList);

        doReturn(preReservationListDto).when(preReservationController).getPreReservationDtos(preReservationList);

        //WHEN
        List<PreReservationDto> preReservationDtoList = preReservationController.listeDesPreReservations();

        //THEN
        assertThat(preReservationController.listeDesPreReservations().get(1).getId_compte()).isEqualTo(1);
        //assertThat(preReservationDtoList.get(1).getId_compte()).isEqualTo(1);
    }

    @Test
    public void listPreReservationsByIdUser_should_throw_IllegalStateException() {
        //GIVEN
        int id_compte = -1;
        List<PreReservation> preReservations = null;

        when(preReservationController.getPrereservationDaoById_compte(id_compte)).thenReturn(preReservations);

        assertThrows(IllegalStateException.class, () -> {
            preReservationController.listPreReservationsByIdUser(id_compte);
        });
    }

    @Test
    public void mapper_should_return_Dto() {
        //GIVEN
        PreReservationMapperImpl preReservationMapper = new PreReservationMapperImpl();

        //WHEN
        PreReservationDto preReservationDto = preReservationMapper.preReservationToPreReservationDto(getPreReservationsDummy().get(0));

        //THEN
        assertThat(preReservationDto.getId_exemplaire()).isEqualTo(getPreReservationDtosDummy().get(0).getId_exemplaire());
        assertThat(preReservationDto.getId_liste_attente()).isEqualTo(getPreReservationDtosDummy().get(0).getId_liste_attente());
    }

    //A revoir il y a un throws de FunctionalException
    @Disabled
    @Test
    public void listPreReservationsByIdUser_should_return_exception() throws FunctionalException {
        //GIVEN
        int id_compte = 99;
        List<PreReservationDto> preReservationDtoList = new ArrayList<>();
        List<PreReservation> preReservationList = new ArrayList<>();
        //WHEN
        when(preReservationController.getPrereservationDaoById_compte(id_compte)).thenReturn(preReservationList);
        //doReturn(preReservationList).when(preReservationController.getPrereservationDaoById_compte(id_compte));
        //doThrow(FunctionalException.class).when(preReservationController.getPreReservationDtos(preReservationList));
        when(preReservationController.getPreReservationDtos(preReservationList)).thenThrow(FunctionalException.class);

        //THEN
        assertThrows(FunctionalException.class, () -> preReservationController.listPreReservationsByIdUser(id_compte));
    }

    @Test
    @DisplayName("addPreReservation_should_return_HttpStatus_CREATED")
    public void addPreReservation_should_HttpStatus_CREATED(){
        //GIVEN
        PreReservation preReservation = getPreReservationsDummy().get(0);
        ResponseEntity<PreReservation> responseEntity = preReservationController.addPreReservation(preReservation);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }

    @Test
    @DisplayName("addPreReservation_should_throw_exception")
    public void addPreReservation_should_throw_exception(){
        //GIVEN
        PreReservation preReservation = null;
        assertThrows(ErrorAddException.class, () -> {preReservationController.addPreReservation(preReservation);});
    }

    @Test
    @DisplayName("updatePreReservation_should_return_HttpStatus_OK")
    public void updatePreReservation_should_return_HttpStatus_OK(){
        //GIVEN
        PreReservation preReservation = getPreReservationsDummy().get(0);
        ResponseEntity<PreReservation> responseEntity = preReservationController.updatePreReservation(preReservation);
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.ACCEPTED);
    }

    @Test
    @DisplayName("updatePreReservation_should_throw_exception")
    public void updatePreReservation_should_throw_exception(){
        //GIVEN
        PreReservation preReservation = null;
        assertThrows(ErrorAddException.class, () -> {preReservationController.updatePreReservation(preReservation);});
    }

    @Test
    public void getPreReservationDto_should_return_PreReservationDto() throws FunctionalException {
        PreReservation preReservation = new PreReservation(1, Date.from(Instant.now()), 1, true, true);
        PreReservationDto preReservationDto = new PreReservationDto(1, 1, Date.from(Instant.now()), 1, true, true);

        when(preReservationController.getPreReservationDto(java.util.Optional.of(preReservation))).thenReturn(preReservationDto);

        preReservationController.getPreReservationDto(java.util.Optional.of(preReservation));

        assertThat(preReservationController.getPreReservationDto(java.util.Optional.of(preReservation))).isEqualTo(preReservationDto);
    }

    @Test
    public void getPreReservationDtos_should_return_PreReservationDtoList() throws FunctionalException {

        List<PreReservation> preReservationList = new ArrayList<>();
        List<PreReservationDto> preReservationDtoList = new ArrayList<>();

        PreReservation preReservation = getPreReservationsDummy().get(0);
        PreReservationDto preReservationDto = getPreReservationDtosDummy().get(0);

        preReservationList.add(preReservation);
        preReservationList.add(preReservation);
        preReservationDtoList.add(preReservationDto);
        preReservationDtoList.add(preReservationDto);

        when(preReservationController.preReservationMapper.preReservationToPreReservationDto(preReservation)).thenReturn(preReservationDto);

        preReservationController.getPreReservationDtos(preReservationList);

        assertThat(preReservationController.getPreReservationDtos(preReservationList)).isEqualTo(preReservationDtoList);
    }

    @Test
    public void getPreReservationDtos_should_be_empty() throws FunctionalException {
        List<PreReservation> vide = new ArrayList<>();
        assertThrows(FunctionalException.class, () -> preReservationController.getPreReservationDtos(vide));
    }

    @Test
    public void expirationDateCheck_should_return_true(){
        //GIVEN
        LocalDate localDate = LocalDate.now().minusDays(3);
        Date dateToTest =Date.valueOf(localDate);

        //WHEN
        boolean expired = preReservationController.expirationDateCheck(dateToTest);

        //THEN
        assertThat(expired).isEqualTo(true);
    }

    @Test
    public void expirationDateCheck_should_return_false(){
        //GIVEN
        LocalDate localDate = LocalDate.now().minusDays(1);
        Date dateToTest =Date.valueOf(localDate);

        //WHEN
        boolean expired = preReservationController.expirationDateCheck(dateToTest);

        //THEN
        assertThat(expired).isEqualTo(false);
    }

    @Test
    public void getExpiredMailSendPreReservation_should_return_list(){

        //GIVEN
        PreReservation preReservation = new PreReservation(1, Date.valueOf("2020-1-1"), 1, false, false);
        preReservation.setDateMailSend(Date.valueOf("2020-1-1"));
        List<PreReservation> preReservationListExpired = new ArrayList<>();
        preReservationListExpired.add(preReservation);

        //WHEN
        when(preReservationDao.findAllByNotExpired()).thenReturn(preReservationListExpired);
        List<PreReservation> preReservationList = preReservationController.getExpiredMailSendPreReservation();

        //THEN
        assertThat(preReservationList).isEqualTo(preReservationListExpired);

    }

    @Test
    public void getExpiredMailSendPreReservation_should_return_list_null(){

        //GIVEN
        List<PreReservation> preReservationListExpired = new ArrayList<>();

        //WHEN
        when(preReservationDao.findAllByNotExpired()).thenReturn(preReservationListExpired);
        List<PreReservation> preReservationList = preReservationController.getExpiredMailSendPreReservation();

        //THEN
        assertThat(preReservationList).isEqualTo(preReservationListExpired);

    }


    //DUMMY------------------------------------------------

    @NotNull
    protected List<PreReservationDto> getPreReservationDtosDummy() {
        PreReservationDto preReservationDto1 = new PreReservationDto(1, 1, Date.from(Instant.now()), 1, false, false);
        preReservationDto1.setId_exemplaire(1);
        PreReservationDto preReservationDto2 = new PreReservationDto(2,1, Date.from(Instant.now()), 2, false, false);
        preReservationDto2.setId_exemplaire(3);
        List<PreReservationDto> preReservationListDto = new ArrayList<PreReservationDto>();
        preReservationListDto.add(preReservationDto1);
        preReservationListDto.add(preReservationDto2);
        return preReservationListDto;
    }

    @NotNull
    protected List<PreReservation> getPreReservationsDummy() {
        PreReservation preReservation1 = new PreReservation(1, Date.from(Instant.now()), 1, false, false);
        preReservation1.setId_prereservation(1);
        preReservation1.setId_exemplaire(1);
        PreReservation preReservation2 = new PreReservation(1, Date.from(Instant.now()), 2, false, false);
        preReservation1.setId_prereservation(1);
        preReservation2.setId_exemplaire(3);
        List<PreReservation> preReservationList = new ArrayList<PreReservation>();
        preReservationList.add(preReservation1);
        preReservationList.add(preReservation2);
        return preReservationList;
    }
}
